package com.atlassian.fastdev;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.util.Map;
import java.util.Set;

import com.atlassian.fastdev.util.Option;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.collect.ImmutableMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.fastdev.util.Option.none;
import static com.atlassian.fastdev.util.Option.some;

public class ReloadHandler
{
    private final Logger LOG = LoggerFactory.getLogger(ReloadHandler.class);
    private final PluginReloader pluginReloader;
    private final TemplateRenderer renderer;
    private final TargetScanner targetScanner;
    private final ApplicationProperties applicationProperties;
    private final FastdevProperties fastdevProperties;

    public ReloadHandler(TemplateRenderer renderer,
                         TargetScanner targetScanner,
                         PluginReloader pluginReloader,
                         ApplicationProperties applicationProperties,
                         FastdevProperties fastdevProperties)
    {
        this.renderer = renderer;
        this.targetScanner = targetScanner;
        this.pluginReloader = pluginReloader;
        this.applicationProperties = applicationProperties;
        this.fastdevProperties = fastdevProperties;
    }

    /**
     * Scans plugin source code for changes that would require a plugin reload.
     * If changes are detected, the changed plugins are reloaded.
     *
     * @return the body of an HTML document for tracking reload progress, or {@code none()} if no changes were detected and no tasks are in progress.
     * @throws IOException if there is a problem rendering output
     */
    public Option<String> reloadPlugins() throws IOException
    {
        Set<ScanResult> results = targetScanner.scan();
        if (!results.isEmpty())
        {
            LOG.info("Detected changes that require plugin reload, reloading...");
            Map<String, Object> context = ImmutableMap.<String,Object>of(
                "baseurl", URI.create(applicationProperties.getBaseUrl()).normalize().toASCIIString(),
                "version", fastdevProperties.getFastdevVersion());
            StringWriter output = new StringWriter();
            renderer.render("fastdev-output.vm", context, output);
            for (ScanResult result : results)
            {
                File root = result.getRoot();
                LOG.info("Reloading plugin at " + root.getPath());
                pluginReloader.pluginInstall(result);
            }
            return some(output.toString());
        }
        else
        {
            LOG.info("Detected no changes that require plugin reload.  Not reloading.");
            return none();
        }
    }

    /**
     * Scans plugin source code for changes that would require a plugin reload.
     * If changes are detected, the changed plugins are reloaded along with the integration tests bundle
     *
     * @return the body of an HTML document for tracking reload progress, or {@code none()} if no changes were detected and no tasks are in progress.
     * @throws IOException if there is a problem rendering output
     */
    public Option<String> reloadPluginsWithTests() throws IOException
    {
        Set<ScanResult> results = targetScanner.scan();
        if (!results.isEmpty())
        {
            LOG.info("Detected changes that require plugin reload, reloading...");
            Map<String, Object> context = ImmutableMap.<String,Object>of(
                    "baseurl", URI.create(applicationProperties.getBaseUrl()).normalize().toASCIIString(),
                    "version", fastdevProperties.getFastdevVersion());
            StringWriter output = new StringWriter();
            renderer.render("fastdev-output.vm", context, output);
            for (ScanResult result : results)
            {
                File root = result.getRoot();
                LOG.info("Reloading plugin at " + root.getPath());
                pluginReloader.pluginInstallWithTests(result);
            }
            return some(output.toString());
        }
        else
        {
            LOG.info("Detected no changes that require plugin reload.  Not reloading.");
            return none();
        }
    }
}
