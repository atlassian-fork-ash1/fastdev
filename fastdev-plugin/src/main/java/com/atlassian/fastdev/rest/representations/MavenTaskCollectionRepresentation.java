package com.atlassian.fastdev.rest.representations;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import com.atlassian.fastdev.maven.MavenTask;
import com.atlassian.fastdev.maven.MavenTaskManager;
import com.atlassian.fastdev.rest.FastdevUriBuilder;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class MavenTaskCollectionRepresentation
{
    @JsonProperty private final Collection<MavenTaskSummaryRepresentation> tasks;
    @SuppressWarnings("unused")
    @JsonProperty private final Map<String, URI> links;

    @JsonCreator
    public MavenTaskCollectionRepresentation(@JsonProperty("tasks") Collection<MavenTaskSummaryRepresentation> tasks,
                                             @JsonProperty("links") Map<String, URI> links)
    {
        this.tasks = ImmutableList.copyOf(tasks);
        this.links = ImmutableMap.copyOf(links);
    }

    public MavenTaskCollectionRepresentation(MavenTaskManager taskManager, final FastdevUriBuilder uriBuilder)
    {
        this.tasks = ImmutableList.copyOf(Iterables.transform(taskManager.getAllTasks(),
                new Function<MavenTask, MavenTaskSummaryRepresentation>() {
                    public MavenTaskSummaryRepresentation apply(MavenTask mavenTask) {
                        return new MavenTaskSummaryRepresentation(mavenTask, uriBuilder);
                    }
                }));
        this.links = ImmutableMap.of("self", uriBuilder.buildMavenTaskCollectionUri());
    }

    /**
     * Returns the number of Maven tasks currently in progress.
     * This method is intended for use by integration tests.
     */
    public int getTaskCount()
    {
        return tasks.size();
    }

    @SuppressWarnings("unused")
    public static class MavenTaskSummaryRepresentation
    {
        @JsonProperty private final Map<String, URI> links;
        @JsonProperty private final String id;
        @JsonProperty private final Long averageTaskTime;
        @JsonProperty private final Long elapsedTime;
        @JsonProperty private final String buildRoot;

        @JsonCreator
        public MavenTaskSummaryRepresentation(@JsonProperty("links") Map<String, URI> links,
                                              @JsonProperty("id") String id,
                                              @JsonProperty("averageTaskTime") Long averageTaskTime,
                                              @JsonProperty("buildRoot") String buildRoot,
                                              @JsonProperty("elapsedTime") Long elapsedTime)
        {
            this.links = ImmutableMap.copyOf(links);
            this.id = id;
            this.averageTaskTime = averageTaskTime;
            this.buildRoot = buildRoot;
            this.elapsedTime = elapsedTime;
        }

        public MavenTaskSummaryRepresentation(MavenTask task, FastdevUriBuilder uriBuilder)
        {
            this.links = ImmutableMap.of("self", uriBuilder.buildMavenTaskUri(task));
            this.id = task.getUuid().toString();
            this.averageTaskTime = task.getAverageTaskTime();
            this.buildRoot = task.getBuildRoot().getAbsolutePath();
            this.elapsedTime = task.getElapsedTime();
        }

    }
}
