package com.atlassian.fastdev.maven;

/**
 * @since version
 */
public class CLIProcess
{
    private Integer port;
    private Process process;

    public CLIProcess(Integer port, Process process)
    {
        this.port = port;
        this.process = process;
    }

    public Integer getPort()
    {
        return port;
    }

    public Process getProcess()
    {
        return process;
    }
}
