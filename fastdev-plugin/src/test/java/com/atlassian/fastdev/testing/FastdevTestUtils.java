package com.atlassian.fastdev.testing;

import java.io.File;
import java.io.IOException;

import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.writeStringToFile;

public final class FastdevTestUtils
{
    public static final String SERVLET1_URI = "/plugins/servlet/test";
    public static final String SERVLET2_URI = "/plugins/servlet/test2";
    public static final String BUNDLE_FILENAME = "fastdev-test-bundle/src/main/java/com/atlassian/labs/test/api/HelloWorld.java";
    public static final String SERVLET2_FILENAME = "fastdev-test-plugin2/src/main/java/com/atlassian/labs/test/ExampleServlet.java";
    public static final String SERVLET1_FILENAME = "fastdev-test-plugin/src/main/java/com/atlassian/labs/test/ExampleServlet.java";

    public static void waitUntilTasksAreCompleted(RestTester restTester) throws InterruptedException
    {
        while (restTester.getNumPendingTasks() > 0)
        {
            Thread.sleep(1500);
        }
    }

    public static void touchFile(String filename)
    {
        File sourceFile = getSourceFile(filename);
        if (!sourceFile.setLastModified(System.currentTimeMillis()))
        {
            throw new RuntimeException("Failed to update last modification time for " + sourceFile.getAbsolutePath());
        }
    }

    public static void replaceStringInFile(String filename, String from, String to) throws IOException
    {
        File file = getSourceFile(filename);
        String source = readFileToString(file);
        source = source.replace(from, to);
        writeStringToFile(file, source);
    }

    private static File getSourceFile(String filename)
    {
        return new File("../" + filename);
    }
}
