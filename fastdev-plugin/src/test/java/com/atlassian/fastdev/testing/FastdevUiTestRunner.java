package com.atlassian.fastdev.testing;

import com.atlassian.integrationtesting.ApplicationPropertiesImpl;
import com.atlassian.integrationtesting.runner.ApplicationLogDelimiter;
import com.atlassian.integrationtesting.runner.CompositeTestRunner;
import com.atlassian.integrationtesting.runner.TestGroupRunner;
import com.atlassian.integrationtesting.ui.RunningTestGroup;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.ConfigurationException;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.spi.Message;
import org.junit.runners.model.InitializationError;

import static org.apache.commons.lang.StringUtils.isBlank;

public final class FastdevUiTestRunner extends CompositeTestRunner
{
    public FastdevUiTestRunner(Class<?> klass) throws InitializationError
    {
        super(klass, compose());
    }

    public static Composer compose()
    {
        Injector injector = Guice.createInjector(new FastdevUiModule());
        return CompositeTestRunner.compose().
            from(TestGroupRunner.compose()).
            from(ApplicationLogDelimiter.compose(injector)).
            beforeTestClass(new InjectStatics(injector));
    }

    static final class FastdevUiModule extends AbstractModule
    {
        @Override
        protected void configure() {}

        @Provides @Singleton @RunningTestGroup String getRunningTestGroup()
        {
            if (isBlank(TestGroupRunner.getRunningTestGroup()))
            {
                throw new ConfigurationException(ImmutableList.of(new Message("No testGroup configured")));
            }
            return TestGroupRunner.getRunningTestGroup();
        }

        @Provides @Singleton ApplicationProperties getApplicationProperties()
        {
            return ApplicationPropertiesImpl.getStandardApplicationProperties();
        }

        @Provides @Singleton RestTester getRestTester()
        {
            return new RestTester();
        }

        @Provides @Singleton TestedProduct<WebDriverTester> getTestedProduct()
        {
            return TestedProductFactory.create(FastdevTestedProduct.class, getRunningTestGroup(), null);
        }
    }

    private static final class InjectStatics implements Function<BeforeTestClass, Void>
    {
        private final Injector injector;

        public InjectStatics(Injector injector)
        {
            this.injector = injector;
        }

        public Void apply(BeforeTestClass test)
        {
            injector.createChildInjector(new StaticInjectionModule(test.testClass.getJavaClass()));
            return null;
        }

        private static final class StaticInjectionModule extends AbstractModule
        {
            private final Class<?> testClass;

            public StaticInjectionModule(Class<?> testClass)
            {
                this.testClass = testClass;
            }

            @Override
            protected void configure()
            {
                requestStaticInjection(testClass);
            }
        }
    }
}
