package com.atlassian.fastdev;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestTargetScanner
{
    private File root1File;
    private ScanResult root1;
    private ScanResult root1WithPomChange;
    private File root2File;
    private ScanResult root2;
    private File root1SvnFile;
    private ScanResult root1Svn;
    private TargetScanner scanner;

    @Before
    public void setUp() throws Exception
    {
        File res1 = new File("target/root1/src/main/resources").getCanonicalFile();
        res1.mkdirs();
        root1File = new File("target/root1/src/main").getCanonicalFile();
        root1 = new ScanResult(root1File.getParentFile().getParentFile(),root1File,false,new File(root1File.getParentFile().getParentFile(),"pom.xml"));
        root1WithPomChange = new ScanResult(root1File.getParentFile().getParentFile(),root1File,true,new File(root1File.getParentFile().getParentFile(),"pom.xml"));
        root1SvnFile = new File(root1File, ".svn");
        root1SvnFile.mkdir();
        root1Svn = new ScanResult(root1SvnFile.getParentFile().getParentFile(),root1SvnFile,false,new File(root1SvnFile.getParentFile().getParentFile(),"pom.xml"));
        
        File res2 = new File("target/root2/src/main/resources").getCanonicalFile();
        res2.mkdirs();
        
        root2File = new File("target/root2/src/main").getCanonicalFile();
        root2 = new ScanResult(root2File.getParentFile().getParentFile(),root2File,false,new File(root2File.getParentFile().getParentFile(),"pom.xml"));
        
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, res1.getPath() + "," + res2.getPath());
        scanner = new TargetScanner();
    }

    @After
    public void tearDown() throws Exception
    {
        System.setProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES, "");
        FileUtils.cleanDirectory(root1File);
        FileUtils.cleanDirectory(root2File);
    }

    @Test
    public void testScanWithJustDirectory()
    {
        assertTrue(scanner.scan().isEmpty());
    }

    @Test
    public void testScanWithNewFile() throws IOException
    {
        scanner.scan();
        File file = new File(root1File, "bob");
        FileUtils.writeStringToFile(file, "hi");
        assertEquals(ImmutableSet.of(root1), scanner.scan());
        assertEquals(ImmutableSet.of(), scanner.scan());
    }

    @Test
    public void testScanWithChangedFile() throws IOException, InterruptedException
    {
        scanner.scan();
        File file = new File(root1File, "bob");
        FileUtils.writeStringToFile(file, "hi");
        assertEquals(ImmutableSet.of(root1), scanner.scan());
        Thread.sleep(1000);
        FileUtils.writeStringToFile(file, "jim");
        assertEquals(ImmutableSet.of(root1), scanner.scan());
    }

    @Test
    public void testScanWithDeletedFile() throws IOException, InterruptedException
    {
        scanner.scan();
        File file = new File(root1File, "bob");
        FileUtils.writeStringToFile(file, "hi");
        assertEquals(ImmutableSet.of(root1), scanner.scan());
        Thread.sleep(1000);
        file.delete();
        assertEquals(ImmutableSet.of(root1), scanner.scan());
    }

    @Test
    public void testScanWithChangedFileInDirectory() throws IOException, InterruptedException
    {
        scanner.scan();
        File dir = new File(root1File, "dir");
        File file = new File(dir, "bob");
        FileUtils.writeStringToFile(file, "hi");
        assertEquals(ImmutableSet.of(root1), scanner.scan());
        Thread.sleep(1000);
        FileUtils.writeStringToFile(file, "jim");
        assertEquals(ImmutableSet.of(root1), scanner.scan());
    }

    @Test
    public void testScanWithTwoNewFiles() throws IOException, InterruptedException
    {
        scanner.scan();
        File file1 = new File(root1File, "bob");
        File file2 = new File(root2File, "bob");
        FileUtils.writeStringToFile(file1, "hi");
        FileUtils.writeStringToFile(file2, "hi");
        assertEquals(new HashSet<ScanResult>(ImmutableSet.of(root1, root2)), scanner.scan());
    }

    @Test
    public void assertThatScanWithOnlySvnChangeFindsNoChange() throws IOException
    {
        scanner.scan();
        File file = new File(root1SvnFile, "bob");
        FileUtils.writeStringToFile(file, "hi");
        assertEquals(ImmutableSet.of(), scanner.scan());
    }

    @Test
    public void assertThatScanWithChangeToSourceAndSvnFindsChange() throws IOException
    {
        scanner.scan();
        File file1 = new File(root1SvnFile, "bob");
        File file2 = new File(root1File, "bob");
        FileUtils.writeStringToFile(file1, "hi");
        FileUtils.writeStringToFile(file2, "hi");
        assertEquals(ImmutableSet.of(root1), scanner.scan());
    }
    
    @Test
    public void assertThatScanDetectsChangeToPomFile() throws IOException
    {
        scanner.scan();
        File pom = root1.getRootPom();
        FileUtils.writeStringToFile(pom, "Welcome, Great Pumpkin");
        assertEquals((ImmutableSet.of(root1WithPomChange)), scanner.scan());
    }
}
